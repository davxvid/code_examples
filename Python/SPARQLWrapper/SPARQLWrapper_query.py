#!/usr/bin/env python
# coding: utf-8
# python3.4+ !!!

import re
import urllib
import sys
import csv
from slugify import slugify # !!! use awsome-slugify !!!
from SPARQLWrapper import SPARQLWrapper, JSON


##############################################################################################
#
# Fonction qui prend des données xml issues de sitadelXMLscrap et renvoie du rdf au format ttl
#
##############################################################################################

def csv2ttl(csvPath) :

    
    csvFile = open(csvPath, 'r', encoding='latin1') 
    csvReader = csv.reader(csvFile, delimiter=';')

# On récupère la date de la série de données
    year = ''
    month = ''
    date = ''

    reDate = re.compile('Période - (.*)')
    dateLabel = reDate.match(next(csvReader)[0]).group(1)
    matchDate = re.match('(.*?) (.*)',dateLabel) 

    
    if re.match('.*Cumul.*',dateLabel) : # On vérifie que la série récupérée est bien mensuelle (pas annuelle ou trimestrielle)
        sys.exit(0)  # sinon on arrête tout
    else :
        year =  matchDate.group(2)
        month =  matchDate.group(1)        
        date = year+'-'+str(dicMonthToNumberFr[month])
    graph = "<http://dataxid.org/data/open/developpement-durable.bsocom.fr/Construction/Sitadel2/***/data"+date+">"
    query = "PREFIX gm: <http://dataxid.org/schema/generic_metadata#> PREFIX schema: <http://dataxid.org/data/open/developpement-durable.bsocom.fr/Construction/Sitadel2/***/schema#> PREFIX data: <http://dataxid.org/data/open/developpement-durable.bsocom.fr/Construction/Sitadel2/***/data#> INSERT DATA { GRAPH "+graph+" { \n"
# On récupère les entêtes du csv et on stocke des versions suggifiées dans une liste
    headers=[]
    next(csvReader)
    for i, label in enumerate(next(csvReader)) :
        headers.insert(i, slugify(label))
    headers[0] = "placeLabel"

#On parcours les données et on crée la requête
    for row in csvReader :
        placeLabel = row[0]
        placeId = ""
        try: 
            placeId = dicDep[placeLabel] # On vérifie que la ligne décrit un département
        except KeyError:
            continue # sinon on passe à la ligne suivante

        for i in range(1, len(row)):
            dataId = 'data:'+date+'_'+headers[i]+'_'+placeId
            print(dataId)
            query += dataId+' a schema:'+headers[i]+', gm:donnee_mensuelle ; gm:date "'+date+'" ; gm:date_label "'+dateLabel+'" ; gm:valeur "'+row[i]+'" .\n <http://id.insee.fr/geo/departement/'+placeId+'> gm:est_decrit_par '+dataId+' .\n'
    query += "}}"
    return(query)

##############################################################################################
##############################################################################################




##############################################################################################
#
# Déclaration de dictionaires utiles
#
##############################################################################################

dicMonthToNumberFr = {"janvier":"01", "février":"02", "mars":"03", "avril":"04", "mai":"05", "juin":"06", "juillet":"07", "août":"08", "septembre":"09", "octobre":"10", "novembre":"11", "décembre":"12"}

dicDep = {"Ain":"01",
"Aisne":"02",
"Allier":"03",
"Alpes-Maritimes":"06",
"Alpes-de-Haute-Provence":"04",
"Ardennes":"08",
"Ardèche":"07",
"Ariège":"09",
"Aube":"10",
"Aude":"11",
"Aveyron":"12",
"Bas-Rhin":"67",
"Bouches-du-Rhône":"13",
"Calvados":"14",
"Cantal":"15",
"Charente":"16",
"Charente-Maritime":"17",
"Cher":"18",
"Corrèze":"19",
"Corse-du-Sud":"2A",
"Creuse":"23",
"Côte-d'Or":"21",
"Côtes-d'Armor":"22",
"Deux-Sèvres":"79",
"Dordogne":"24",
"Doubs":"25",
"Drôme":"26",
"Essonne":"91",
"Eure":"27",
"Eure-et-Loir":"28",
"Finistère":"29",
"Gard":"30",
"Gers":"32",
"Gironde":"33",
"Guadeloupe":"971",
"Guyane":"973",
"Haut-Rhin":"68",
"Haute-Corse":"2B",
"Haute-Garonne":"31",
"Haute-Loire":"43",
"Haute-Marne":"52",
"Haute-Savoie":"74",
"Haute-Saône":"70",
"Haute-Vienne":"87",
"Hautes-Alpes":"05",
"Hautes-Pyrénées":"65",
"Hauts-de-Seine":"92",
"Hérault":"34",
"Ille-et-Vilaine":"35",
"Indre":"36",
"Indre-et-Loire":"37",
"Isère":"38",
"Jura":"39",
"La Réunion":"974",
"Landes":"40",
"Loir-et-Cher":"41",
"Loire":"42",
"Loire-Atlantique":"44",
"Loiret":"45",
"Lot":"46",
"Lot-et-Garonne":"47",
"Lozère":"48",
"Maine-et-Loire":"49",
"Manche":"50",
"Marne":"51",
"Martinique":"972",
"Mayenne":"53",
"Mayotte":"976",
"Meurthe-et-Moselle":"54",
"Meuse":"55",
"Morbihan":"56",
"Moselle":"57",
"Nièvre":"58",
"Nord":"59",
"Oise":"60",
"Orne":"61",
"Paris":"75",
"Pas-de-Calais":"62",
"Puy-de-Dôme":"63",
"Pyrénées-Atlantiques":"64",
"Pyrénées-Orientales":"66",
"Rhône":"69",
"Sarthe":"72",
"Savoie":"73",
"Saône-et-Loire":"71",
"Seine-Maritime":"76",
"Seine-Saint-Denis":"93",
"Seine-et-Marne":"77",
"Somme":"80",
"Tarn":"81",
"Tarn-et-Garonne":"82",
"Territoire de Belfort":"90",
"Val-d'Oise":"95",
"Val-de-Marne":"94",
"Var":"83",
"Vaucluse":"84",
"Vendée":"85",
"Vienne":"86",
"Vosges":"88",
"Yonne":"89",
"Yvelines":"78" }

##############################################################################################





filename = 'dpc-loc-04_98777429270282'

query = csv2ttl(filename+'.csv')

outfile = open(filename+'.rq','w')

outfile.write(query)
#endpoint = "http://fuseki-shared0:3030/sitadel-test/query"
#sparql = SPARQLWrapper(endpoint)
#sparql.setQuery(query)
#sparql.query()





 #   for index, item in enumerate(items, start=4):
    
    


