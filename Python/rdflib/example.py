import rdflib
import rdfextras
rdfextras.registerplugins() # so we can Graph.query()
import csv
import re
from agrobaseExtractor.utils import variables as VAR


g=rdflib.Graph()
g.parse(VAR.GHS_SCHEMA, format="turtle")

def codeList2pictograms(job) :
    (prodId,codeList)=job
    pictoResultList = []
    resultStr = ''
    queryPicto = """SELECT ?picto
WHERE {{
?prodClass <http://dataxid.org/dev/GHS_Rev.7_2017/schema#has_statement> <http://dataxid.org/dev/GHS_Rev.7_2017/schema#{}> ; <http://dataxid.org/dev/GHS_Rev.7_2017/schema#has_pictogram> ?picto 

}}
    """
    for codeComp in codeList :
        if re.search('^H[0-9][0-9][0-9]',codeComp):
            for code in codeComp.split('+') :
                pictoResult = g.query(queryPicto.format(code))                      
                pictoList = [str(r.asdict()['picto']).split('#')[-1] for r in pictoResult]
                pictoListClean = [x for x in pictoList if x != 'GHS-NONE']
                try : pictoResultList.append(pictoListClean[0]) 
                except : pass
    for picto in list(set(pictoResultList)) :
        resultStr += prodId+'\t'+picto+'\t'+'Picto infered from retrieved hazard codes'+'\t\tFalse\t\t\t\n'
    return resultStr 

